# Installation

## Download VirtualBox and Vagrant 
- https://www.virtualbox.org/wiki/Downloads
- https://www.vagrantup.com/downloads

## should install this plugin
```vagrant plugin install vagrant-hostmanager```
https://soyuka.me/creer-un-environnement-de-developpement-avec-vagrant-et-chef/

# Main command : Vagrant
- vagrant up
- vagrant provision
- vagrant destroy

# ssh configuration

- cf : https://guillaumebriday.fr/utiliser-la-commande-ssh-pour-entrer-dans-une-machine-vagrant

- vagrant ssh-config

- put this block in ~/.ssh/config
> Host vagrant-proxy
  HostName 192.168.33.10 (cf vagrant/Vagrantfile)
  User vagrant
  Port 22
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentityFile /.../.../Vagrant/.vagrant/machines/default/virtualbox/private_key
  IdentitiesOnly yes
  LogLevel FATAL
  
  > Host vagrant-server
  HostName 192.168.33.11 (cf vagrant/Vagrantfile)
  User vagrant
  Port 22
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentityFile /.../.../Vagrant/.vagrant/machines/default/virtualbox/private_key
  IdentitiesOnly yes
  LogLevel FATAL